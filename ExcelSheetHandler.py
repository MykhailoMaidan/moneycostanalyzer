from openpyxl import Workbook
from openpyxl.utils import get_column_letter

class ExcelSheetHandler:
    def __init__(self, sheetName = "Money"):
        self.workBook = Workbook()
        self.sheetName = sheetName
