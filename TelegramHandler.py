import telebot
from ExcelSheetHandler import ExcelSheetHandler

class TelegramHandler:
    def __init__(self,telegramBotObject:telebot.TeleBot):
        self.userDict = {}
        self.botObject = telegramBotObject

    def startCmd(self,userId:int):
        if userId in self.userDict:
            self.botObject.send_message(userId,"Hello user. I saw you before.")
        else:
            self.botObject.send_message(userId,"Hello new user.")
            self.userDict[userId] = ExcelSheetHandler("Sheet.xlsx")
